const getProductosAsyncAwait = async () => {
    const response = await fetch(`https://services.odata.org/V4/Northwind/Northwind.svc/Products`)
    const json = await response.json();
    console.log("async/await based");
    console.table(json.value);
}
