let clientesObtenidos;

function getClientes(){
  let url = "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  let request = new XMLHttpRequest();

  request.onreadystatechange = function(){
    if (this.readyState==4 && this.status == 200) {
        //console.table(JSON.parse(request.responseText).value);
        clientesObtenidos = request.responseText;
        procesarClientes();
    }
  }

  request.open("GET", url, true);
  request.send();
}
function procesarClientes(){
  let rutaBandera="https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  let JSONClientes = JSON.parse(clientesObtenidos)
  let divTabla = tablaClientes;
  let tabla = document.createElement('table');
  let tbody = document.createElement('tbody');

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (client of JSONClientes.value) {
    console.log(client);
    let nuevaFila = document.createElement("tr");
    let columnaNombre = document.createElement("td");
    columnaNombre.innerText = client.ContactName;
    let columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = client.City;
    let columnaBandera = document.createElement("td");
    let imgBandera = document.createElement("img");
    imgBandera.classList.add("flag");
    if (client.Country==='UK') {
      imgBandera.src = `${rutaBandera}United-Kingdom.png`
    }else {
      imgBandera.src = `${rutaBandera}${client.Country}.png`
    }
    columnaBandera.appendChild(imgBandera);

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaBandera);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
