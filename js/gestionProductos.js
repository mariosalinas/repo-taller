let productosObtenidos;

function getProductos(){
  let url = "https://services.odata.org/V4/Northwind/Northwind.svc/Products";
  let request = new XMLHttpRequest();

  request.onreadystatechange = function(){
    if (this.readyState==4 && this.status == 200) {
        // console.table(JSON.parse(request.responseText).value);
        productosObtenidos = request.responseText;
        procesarProductos();
    }
  }

  request.open("GET", url, true);
  request.send();
}

function procesarProductos(){
  let JSONProductos = JSON.parse(productosObtenidos)
  let divTabla = tablaProductos;
  let tabla = document.createElement('table');
  let tbody = document.createElement('tbody');

  tabla.classList.add("table");
  tabla.classList.add("table-striped");

  for (product of JSONProductos.value) {
    console.log(product.ProductName);
    let nuevaFila = document.createElement("tr");
    let columnaNombre = document.createElement("td");
    columnaNombre.innerText = product.ProductName;
    let columnaPrecio = document.createElement("td");
    columnaPrecio.innerText = product.UnitPrice;
    let columnaStock = document.createElement("td");
    columnaStock.innerText = product.UnitsInStock;

    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaPrecio);
    nuevaFila.appendChild(columnaStock);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.appendChild(tabla);
}
